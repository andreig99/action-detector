import numpy as np
import re
import os
from sklearn import neural_network

etichete_pers = np.array([])
filenames = np.array([])
for filename in os.listdir('DATA'):
    if filename.endswith(".txt"): 
          filenames = np.append(filenames, os.path.join('DATA', filename))
          etichete_pers = np.append(etichete_pers, filename[0 : -4])

for i in range(np.shape(etichete_pers)[0]):
    if etichete_pers[i][-1].isdigit():
        etichete_pers[i] = etichete_pers[i][0 : -1]
    elif etichete_pers[i][-1] == ']':
        etichete_pers[i] = etichete_pers[i][0 : -9]

etichete_l = []

with open('OUTPUT/out.txt', 'w') as outfile:
    for fname in filenames:
        with open(fname) as infile:
            for line in infile:
                outfile.write(line)
                etichete_l.append(etichete_pers[np.where(filenames == fname)[0]].tolist())
            outfile.write('\n')

for i in range(len(etichete_l)):
    if etichete_l[i] == ['Bowing']:
        etichete_l[i] = ['Normal']
    elif etichete_l[i] == ['Clapping']:
        etichete_l[i] = ['Normal']
    elif etichete_l[i] == ['Handshaking']:
        etichete_l[i] = ['Normal']
    elif etichete_l[i] == ['Hugging']:
        etichete_l[i] = ['Normal']
    elif etichete_l[i] == ['Jumping']:
        etichete_l[i] = ['Normal']
    elif etichete_l[i] == ['Running']:
        etichete_l[i] = ['Normal']
    elif etichete_l[i] == ['Seating']:
        etichete_l[i] = ['Normal']
    elif etichete_l[i] == ['Standing']:
        etichete_l[i] = ['Normal']
    elif etichete_l[i] == ['Walking']:
        etichete_l[i] = ['Normal']
    elif etichete_l[i] == ['Waving']:
        etichete_l[i] = ['Normal']
    else:
        etichete_l[i] = ['Aggressive']

with open('OUTPUT/out.txt', 'rb') as f:
    clean_lines = (re.sub(b' +', b' ', line) for line in f)
    indexed_data = np.genfromtxt(clean_lines, delimiter = ' ')

data = indexed_data[:, 1:]
etichete_arr = np.array(etichete_l)

idx = np.random.permutation(np.shape(data)[0])
date = data[idx]
etichete = etichete_arr[idx]

date_train = date[0 : 454036]
etichete_train = etichete[0 : 454036]

date_test = date[454036 : 605381]
etichete_test = etichete[454036 : 605381]

learning_rate = [0.1, 0.01]
hidden_layer = np.array([[27], [13], [27, 27], [27, 13]], dtype = object)

results = np.array([], dtype = object)

for a in learning_rate:
    for b in hidden_layer:
        clf = neural_network.MLPClassifier(hidden_layer_sizes = b, learning_rate_init = a)
        clf.fit(date_train, etichete_train.ravel())
        predictii = clf.predict(date_test)
        num = 0
        for n in range(np.shape(predictii)[0]):
            if predictii[n] == etichete_test[n]:
                num = num + 1
        acc = num / np.shape(predictii)[0] * 100
        results = np.append(results, [a, b, acc])

print(results)