﻿- Am folosit baza de date:  
[https://archive.ics.uci.edu/ml/datasets/Vicon+Physical+Action+Data+Set#](https://archive.ics.uci.edu/ml/datasets/Vicon+Physical+Action+Data+Set)

- Am redenumit fisierele .txt pentru a le putea pune manual intr-un folder de date.
- Am creat un folder DATA cu toate fisierele necesare.
- Am creat un folder OUTPUT cu out.txt unde vor fi concatenate esantioanele din fiecare  
fisier.

- In bucata de cod de mai jos am introdus in vectorul filenames path-urile tuturor fisierelor  
din DATA iar in etichete\_pers am facut primul pas in a-mi selecta etichetele eliminand de la  
fiecare fisier terminatia .txt

etichete\_pers = np.array([])  
filenames = np.array([])  
for filename in os.listdir('DATA'):  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; if filename.endswith(".txt"):   
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; filenames = np.append(filenames, os.path.join('DATA', filename))  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; etichete\_pers = np.append(etichete\_pers, filename[0 : -4])  

- Am prelucrat numele etichetelor eliminand digit-ul de la capatul numelui lor, digit folosit la  
inceput pentru a le pune manual in folder-ul DATA si in cazul special al unui fisier denumit  
"action [worse]" am eliminat [worse]

for i in range(np.shape(etichete\_pers)[0]):  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; if etichete\_pers[i][-1].isdigit():  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; etichete\_pers[i] = etichete\_pers[i][0 : -1]  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; elif etichete\_pers[i][-1] == ']':  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; etichete\_pers[i] = etichete\_pers[i][0 : -9]  

- Am parcurs fiecare fisier din DATA rand cu rand si am scris totul in out.txt. Am creat lista  
etichete\_l in care am potrivit numarul de etichete cu numarul de linii din out.txt

with open('OUTPUT/out.txt', 'w') as outfile:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; for fname in filenames:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; with open(fname) as infile:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; for line in infile:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; outfile.write(line)  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; etichete\_l.append(etichete\_pers[np.where(filenames == fname)[0]].tolist())  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; outfile.write('\n')  

- Am generat datele din out.txt in indexed\_data eliminand spatiile multiple dintre date care  
duceau la erori in citirea datelor

with open('OUTPUT/out.txt', 'rb') as f:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; clean\_lines = (re.sub(b' +', b' ', line) for line in f)  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; indexed\_data = np.genfromtxt(clean\_lines, delimiter = ' ')

- Am introdus datele in data eliminand indexii de pozitii din indexed\_data si am facut un  
np.array din lista etichete\_l

data = indexed\_data[:, 1:]  
etichete\_arr = np.array(etichete\_l)

- Am generat indexi aleatori deoarece cand am introdus fisierele in DATA acestea au fost  
sortate alfabetic astfel la impartirea datelor in train si test riscam suprainvatarea setului de  
date de antrenament.

idx = np.random.permutation(np.shape(data)[0])  
date = data[idx]  
etichete = etichete\_arr[idx]

- Am facut impartirea in date\_train si date\_test la fel si pentru etichete pastrand proportiile de  
75% 25%.

date\_train = date[0 : 454036]  
etichete\_train = etichete[0 : 454036]

date\_test = date[454036 : 605381]  
etichete\_test = etichete[454036 : 605381]

- Am variat numarul de straturi ascunse, numarul de neuroni din straturile ascunse si rata de  
invatare.

results = np.array([], dtype = object)

for a in learning\_rate:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; for b in hidden\_layer:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; clf = neural\_network.MLPClassifier(hidden\_layer\_sizes = b, learning\_rate\_init = a)  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; clf.fit(date\_train, etichete\_train.ravel())  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; predictii = clf.predict(date\_test)  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; num = 0  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; for n in range(np.shape(predictii)[0]):  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; if predictii[n] == etichete\_test[n]:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; num = num + 1  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; acc = num / np.shape(predictii)[0] \* 100  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; results = np.append(results, [a, b, acc])

- Rezulatul este o matrice cu 8 linii si 3 coloane din care se iau pe rand liniile, in fiecare linie  
fiind reprezentat learing\_rate-ul, numarul de straturi ascunse, numarul de neuroni de pe  
straturile ascunse si acuratetea.







- Un exemplu de rezultat obtinut este urmatorul:

|Learning rate|Numar neuroni straturi ascunse|Acuratete|
| :-: | :-: | :-: |
|0.1|27|51.05289239816314 %|
|0.1|13|49.87148567841686 %|
|0.1|27, 27|50.12851432158314 %|
|0.1|27, 13|50.12851432158314 %|
|0.01|27|94.34404836631536 %|
|0.01|13|92.73712379001618 %|
|0.01|27, 27|53.030493243912915 %|
|0.01|27, 13|99.08553305361922 %|






























